<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 №B-3</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 №B-3</h1></div>
    <div></div>
</header>

<main>
    <?php

    function accessToFile($filename)
    {
        $info = file('users.csv');

        $fu = fopen('users.csv', 'rt');
        flock($fu, LOCK_EX);
        foreach ($info as $k => $user) {

            $data = str_getcsv($user, ';');

            for ($i = 2; $i < count($data); $i++) {
                $dataTemp = explode("\\", $data[$i]);
                $data[$i] = end($dataTemp);

                if (($data[$i] == $filename) && !($data[0] == $_SESSION['user'][0])) {
                    return -1;
                }
                if ($data[$i] == $filename && $data[0] == $_SESSION['user'][0]) {
                    return +1;
                }
            }
        }
        flock($fu, LOCK_UN);
        fclose($fu); // закрываем файл
    }

    if (!isset($_SESSION['user'])) {
        echo '<a href="/php-lab-B3/index.php">Необходима авторизация</a>';
        echo '<div id="logout" style="text-align:start; display: inline-block"><a href="/php-lab-B3/index.php">Назад</a></div>';
        exit();
    }
    if (!@isset($_GET['filename'])) {
        echo '<p class="error">Имя файла не указано!</p>';
        exit();
    }
    $fileName = explode("\\", $_GET['filename']);
    //    echo '<div id="logout"><a href="/php-lab-B3/index.php">Назад</a></div>';
    echo '<br>';

    if (end($fileName) == 'users.csv') {
        echo '<p class="error">Секретная информация!</p>';
        exit();
    }

    if (accessToFile(end($fileName)) == -1) {
        echo '<p class="error">Нет правдоступа!</p>';
        exit();
    } else if (accessToFile(end($fileName)) == +1) {
        echo '<h3>Загруженный вами файл' . (end($fileName)) . '</h3>';
    } else {
        echo '<h3>Файл' . (end($fileName)) . '</h3>';
    }

    echo '<pre id="viewFile">';

    $f = fopen($_GET['filename'], 'rb');

    if (!$f) {
        echo '<p class="error">Файл не найден!</p>';
        exit();
    }

    if ($f) {
        flock($f, LOCK_EX);

        $content = '';
        while (!feof($f)) { // цикл, пока не достигнут конец файла
            $content .= fgets($f); // читаем строку файла
        }

        echo htmlspecialchars($content); // выводим содержимое файла

        flock($f, LOCK_UN);
        fclose($f); // закрываем файл

    } else {
        echo '<p class="error">Ошибка открытия файла ' . $_GET['filename'] . '</p>';
    }

    echo "</pre>";

    echo '<br>';
    echo '<div id="logout" style="text-align:center"><a href="/php-lab-B3/index.php">Назад</a></div>';
    ?>
</main>


<!--<footer>-->
<!--    -->
<!--</footer>-->

</body>
</html>
