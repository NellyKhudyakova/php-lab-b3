
<?php

function makeLink($name, $path)
{
    return '<p class="fileLinkP"><a class="fileLink" href="' . '/php-lab-B3/viewer.php?filename=' . UrlEncode($path) . '\\' . $name . '" target="_blank">Файл ' . $name . '</a></p>';
}

// depth = глубина открытия каталогов - 1
function outDirInfo($name, $path, $depth = 3) {

    echo '<div class="dirBlock"><p class="dirName">Каталог ' . $name . '</p>'; // выводим имя каталога
    $depth = $depth -1; //  глубина входа
    $dir = opendir($path); // открываем каталог

// перебираем элементы каталога пока они не закончатся
//    if ($depth > 0) {


    while (($file = readdir($dir)) !== false && $depth > 0) {
        if (($name == "." || $name == "..") || ($file == "." || $file == "..")) {
            continue;
        }
            if (is_dir($file) ) { // если элемент каталог выводим его содержимое
                outDirInfo($file, $path . '\\' . $file, $depth);
            } else if (is_dir($file)) {
                echo makeLink($file, $path);
            } else if (is_file($path.'\\'.$file)) {
                echo makeLink($file, $path);
            }

    }
//}
    closedir($dir);
    echo '</div>';
}

function makeName($filename) {
    if (!file_exists($_POST['dir-name'])) {
        umask(0);
        mkdir($_POST['dir-name'], 0777, true);
    }
    $ext = explode('.', $filename);
    $ext2 = $ext[1];
    $n = 1;
    while (file_exists($_POST['dir-name'] . '\\' . $n . '.' . $ext2)) {
        $n = $n + 1;
    }
    return $_POST['dir-name'] . '\\' . $n . '.' . $ext2;
}

function updateFileList($filename) {
    $info = file('users.csv');

    $f = fopen('users.csv', 'wb');
    flock($f, LOCK_EX);
    foreach ($info as $k => $user) {

        $data = str_getcsv($user, ';');

        if ($data[0] == $_SESSION['user'][0]) {

            $user = str_replace(array("\r","\n"),"",$user);

            $user .= ';' . $filename . "\n";

        }
        fwrite($f, $user);
    }
    flock($f, LOCK_UN);
    fclose($f);
}

if (isset($_FILES['myFileName'])) {
    if (isset($_FILES['myFileName']['tmp_name'])) {

        foreach( $_FILES['myFileName']['tmp_name'] as $i => $f )
        {
            echo 'Загрузка '.($i + 1).' – отправлено во временный файл '.$f.'; ';
            print_r($_FILES['myFileName']['name'][$i]);
            if ($f) {
                $newName = makeName($_FILES['myFileName']['name'][$i]);
                move_uploaded_file($f, $newName);

//            echo '<p> Name:  ' . ($_FILES['myFileName']['name']) . '</p>'; // изначальный файл
//            echo '<p> makeName:  ' . makeName($_FILES['myFileName']['name']) . '</p>'; // переименованый файл

                updateFileList($newName);

                echo '<p class="ok-uploaded">Файл ' . $_FILES['myFileName']['name'][$i] . ' загружен на сервер</p>';
            } else {
                rmdir($_POST['dir-name']);
            }
        }

    }
}

echo '<div id="dir_tree">';
echo '<h3>Содержимое текущего каталога</h3>';
$dirPath = explode("\\",getcwd());
$dirName = $dirPath[count($dirPath)-1];
outDirInfo("$dirName", getcwd());
echo '</div>';

?>
    <h3>Загрузка файла для редактирования</h3>
<!--<form method="post" enctype="multipart/form-data" action="/php-lab-B3/tree.php" target="_blank">-->
    <form method="post" enctype="multipart/form-data" action="/php-lab-B3/index.php" target="_blank"" >
        <label for="dir-name">Каталог на сервере</label>
        <input type="text" name="dir-name" id="dir-name">

        <label for="myFileName">Локальный файл</label>
        <input type="file" name="myFileName[]" multiple>

        <input type="submit" value="Отправить файл на сервер">
    </form>

