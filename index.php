<!DOCTYPE html>
<html lang="en">
<head>
    <!--    <meta charset="windows-1251">-->
    <meta charset="UTF-8">
    <title>Худякова Нелли Константиновна 181-322 №B-3</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,500" rel="stylesheet">
    <link rel="stylesheet" href="style.css">
</head>
<body>

<header>
    <div class="header-logo"></div>
    <div class="header-heading"><h1>Худякова Нелли Константиновна 181-322 №B-3</h1></div>
    <div></div>
</header>

<main>
    <?php
    session_start();

    if (isset($_GET['logout'])) { // если был переход по ссылке Выход

        unset($_SESSION['user']); // удаляем информацию о пользователе
        header('Location: /php-lab-B3/index.php'); // переадресация на главную страницу
        exit(); //
    }

//------ если аутентификации нет, но переданы данные для ее проведения ------

    if (!isset($_SESSION['user']) && isset($_POST['login']) &&
        isset($_POST['password']) && $f = fopen('users.csv', 'rt')) {

        while (!feof($f)) {
            $test_user = explode(';', fgets($f));
            if (trim($test_user[0]) == $_POST['login']) {

                if (isset($test_user[1]) && trim($test_user[1]) == $_POST['password']) {
                    $_SESSION['user'] = $test_user; //
                    header('Location: /php-lab-B3/'); // редирект на главную
                    exit();
                } else break; // если пароль не совпал прекращаем итерации
            }
        }
        echo '<div class="error">Неверный логин или пароль!</div>';
        fclose($f); // закрываем файл
    }

//------ если аутентификации все еще нет -----------------------------------

    if (!isset($_SESSION['user'])) {
// выводим форму для аутентификации
        echo '<form name="auth" method="post" action="">
               <label for="login">Ваш логин</label>
               <input type="text" name="login" placeholder="Введите логин" ';

        if (isset($_POST['login'])) { //если логин уже вводился ранее и был передан в программу
            echo ' value="' . $_POST['login'] . '"';
        }
        echo '><label for="password">Ваш пароль</label>
               <input type="password" name="password" placeholder="Введите пароль">
               <input type="submit" value="Войти">
              </form>';
    } else { // если аутентификация успешно произведена
        echo '<div id="logout"><a href="/php-lab-B3/index.php/?logout=">Выход</a></div>';
//        print_r($_SESSION['user']);
        echo '<p class="ok">Добро пожаловать, ' . $_SESSION['user'][0] . '!</p>';
        include 'tree.php';
    }
    ?>
</main>


<footer>

</footer>

</body>
</html>